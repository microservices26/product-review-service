package com.microservices.productreview.config;

import com.github.cloudyrock.mongock.migration.MongockLegacyMigration;
import io.changock.runner.core.builder.configuration.LegacyMigrationMappingFields;
import io.changock.runner.spring.util.config.ChangockSpringConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.util.StringUtils;

@Configuration
@ConfigurationProperties("mongock")
@Profile("!test")
public class MongockConf extends ChangockSpringConfiguration {

    public static final String DEFAULT_CHANGELOG_COLLECTION_NAME = "mongockChangeLog";
    public static final String DEFAULT_LOCK_COLLECTION_NAME = "mongockLock";

    private String changeLogCollectionName = DEFAULT_CHANGELOG_COLLECTION_NAME;

    private String lockCollectionName = DEFAULT_LOCK_COLLECTION_NAME;

    private boolean indexCreation = true;

    private com.github.cloudyrock.spring.v5.MongockConfiguration.MongockLegacyMigrationConfig legacyMigration = null;

    public String getChangeLogCollectionName() {
        return changeLogCollectionName;
    }

    public void setChangeLogCollectionName(String changeLogCollectionName) {
        this.changeLogCollectionName = changeLogCollectionName;
    }

    public String getLockCollectionName() {
        return lockCollectionName;
    }

    public void setLockCollectionName(String lockCollectionName) {
        this.lockCollectionName = lockCollectionName;
    }

    public boolean isIndexCreation() {
        return indexCreation;
    }

    public void setIndexCreation(boolean indexCreation) {
        this.indexCreation = indexCreation;
    }

    @Override
    @SuppressWarnings("unchecked")
    public com.github.cloudyrock.spring.v5.MongockConfiguration.MongockLegacyMigrationConfig getLegacyMigration() {
        return legacyMigration;
    }

    public void setLegacyMigration(com.github.cloudyrock.spring.v5.MongockConfiguration.MongockLegacyMigrationConfig legacyMigration) {
        this.legacyMigration = legacyMigration;
    }

    public static boolean isLegacyMigrationValid(com.github.cloudyrock.spring.v5.MongockConfiguration config) {
        return config.getLegacyMigration() == null
                || StringUtils.isEmpty(config.getLegacyMigration().getCollectionName())
                || config.getLegacyMigration().getMappingFields() == null
                || StringUtils.isEmpty(config.getLegacyMigration().getMappingFields().getChangeId())
                || StringUtils.isEmpty(config.getLegacyMigration().getMappingFields().getAuthor());
    }

    public static class MongockLegacyMigrationConfig extends MongockLegacyMigration {

        @Override
        @ConfigurationProperties("mongock.legacy-migration.mapping-fields")
        public LegacyMigrationMappingFields getMappingFields() {
            return super.getMappingFields();
        }
    }
}
