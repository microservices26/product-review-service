package com.microservices.productreview.repository;

import com.microservices.productreview.model.ProductReview;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductReviewRepository extends ReactiveMongoRepository<ProductReview, String> { }
