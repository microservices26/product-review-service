package com.microservices.productreview.service;

import com.microservices.productreview.exception.ProductReviewNotFoundException;
import com.microservices.productreview.model.ProductReview;
import com.microservices.productreview.repository.ProductReviewRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductReviewService {
    private final ProductReviewRepository productReviewRepository;

    public ProductReviewService(ProductReviewRepository productReviewRepository) {
        this.productReviewRepository = productReviewRepository;
    }

    @PreAuthorize("hasRole('USER')")
    public Mono<ProductReview> createReview(ProductReview review) {
        return productReviewRepository.save(review);
    }

    @PreAuthorize("hasRole('USER')")
    public Mono<ProductReview> updateReview(ProductReview review) {
        return find(review.getProductId())
                .flatMap(it-> productReviewRepository.save(review));
    }

    public Flux<ProductReview> retrieveAll() {
        return productReviewRepository.findAll();
    }

    public Mono<ProductReview> find(String id) {
        return productReviewRepository.findById(id)
                .switchIfEmpty(Mono.error(new ProductReviewNotFoundException(id)));
    }

    @PreAuthorize("hasRole('USER')")
    public Mono<Void> delete(String id) {
        return productReviewRepository.existsById(id)
                .filter(it-> it)
                .switchIfEmpty(Mono.error(new ProductReviewNotFoundException(id)))
                .flatMap(it -> productReviewRepository.deleteById(id));
    }
}
