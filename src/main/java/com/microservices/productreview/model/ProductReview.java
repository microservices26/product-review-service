package com.microservices.productreview.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "product-review")
public class ProductReview {
    @Id
    private String productId;
    private Double averageReviewScore;
    private Integer numberOfReviews;

    public ProductReview(String productId, Double averageReviewScore, Integer numberOfReviews) {
        this.productId = productId;
        this.averageReviewScore = averageReviewScore;
        this.numberOfReviews = numberOfReviews;
    }

    public String getProductId() {
        return productId;
    }

    public Double getAverageReviewScore() {
        return averageReviewScore;
    }

    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

}
