package com.microservices.productreview.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v2.decorator.impl.MongockTemplate;
import com.microservices.productreview.model.ProductReview;

import java.util.List;

@ChangeLog(order = "001")
public class DatabaseChangelog {

    public DatabaseChangelog() {
    }

    @ChangeSet(order = "001", id = "initialDataSeeding", author = "Wilson")
    public void initialDataSeeding(MongockTemplate mongockTemplate) {
        List<ProductReview> reviews = List.of(new ProductReview("M20324", 1.2, 2),
                new ProductReview("BB5476", 9.2, 20),
                new ProductReview("H02542", 10.0, 1),
                new ProductReview("FX5635", 10.0, 10));

        reviews.forEach(mongockTemplate::save);
    }
}
