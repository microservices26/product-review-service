package com.microservices.productreview.exception;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class ApiError {
    private LocalDateTime timestamp;
    private HttpStatus status;
    private String error;
    private Map<String, List<String>> fieldErrors;

    public ApiError(LocalDateTime timestamp, HttpStatus status, String error, Map<String, List<String>> fieldErrors) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.fieldErrors = fieldErrors;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public Map<String, List<String>> getFieldErrors() {
        return fieldErrors;
    }
}
