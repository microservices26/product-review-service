package com.microservices.productreview.exception;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Map;

public class ProductReviewNotFoundException extends ApiException {
    public ProductReviewNotFoundException(String id) {
        super(new ApiError(LocalDateTime.now(), HttpStatus.NOT_FOUND, "Product Review Not Found (id: " + id + ")", Map.of()),
                "Product Review Not Found (id: " + id + ")");
    }
}
