package com.microservices.productreview.exception;

public class ApiException extends RuntimeException {
    ApiError error;

    public ApiException(ApiError error, String message) {
        super(message);
        this.error = error;
    }
}

