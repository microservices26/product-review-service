package com.microservices.productreview.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class ErrorHandling {
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(WebExchangeBindException.class)
    ApiError handleConstraintViolationException(WebExchangeBindException exception) {
        BindingResult result = exception.getBindingResult();
        Map<String, List<String>> fieldErrors = result.getFieldErrors().stream().collect(Collectors.toMap(FieldError::getField,
                error -> List.of(Objects.requireNonNull(error.getDefaultMessage())), (List<String> x1, List<String> x2) -> List.of(x1.get(0), x2.get(0))));
        return new ApiError(LocalDateTime.now(), BAD_REQUEST, BAD_REQUEST.getReasonPhrase(), fieldErrors);
    }

    @ResponseBody
    @ExceptionHandler(ApiException.class)
    ResponseEntity<ApiError> handleApiException(ApiException exception) {
        return new ResponseEntity<>(exception.error, exception.error.getStatus());
    }
}
