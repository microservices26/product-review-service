package com.microservices.productreview.controller.resource;

import com.microservices.productreview.model.ProductReview;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ReviewCreateUpdateResource {
    @NotBlank
    private String productId;
    @NotNull
    @Min(0)
    private Double averageReviewScore;
    @NotNull
    @Min(0)
    private Integer numberOfReviews;

    public ProductReview toProductReview() {
        return new ProductReview(productId, averageReviewScore, numberOfReviews);
    }

    public ProductReview toProductReview(String id) {
        return new ProductReview(id, averageReviewScore, numberOfReviews);
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setAverageReviewScore(Double averageReviewScore) {
        this.averageReviewScore = averageReviewScore;
    }

    public void setNumberOfReviews(Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }
}
