package com.microservices.productreview.controller;

import com.microservices.productreview.controller.resource.ReviewCreateUpdateResource;
import com.microservices.productreview.model.ProductReview;
import com.microservices.productreview.service.ProductReviewService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController()
@RequestMapping("/review")
@CrossOrigin
public class ProductReviewController {
    private final ProductReviewService productReviewService;

    public ProductReviewController(ProductReviewService productReviewService) {
        this.productReviewService = productReviewService;
    }

    @PutMapping("")
    public Mono<ProductReview> createReview(@Valid @RequestBody ReviewCreateUpdateResource resource) {
        return productReviewService.createReview(resource.toProductReview());
    }

    @PutMapping("/{id}")
    public Mono<ProductReview> updateReview(@PathVariable String id, @Valid @RequestBody ReviewCreateUpdateResource resource) {
        return productReviewService.updateReview(resource.toProductReview(id));
    }

    @GetMapping("")
    public Flux<ProductReview> retrieveAll() {
        return productReviewService.retrieveAll();
    }

    @GetMapping("/{id}")
    public Mono<ProductReview> find(@PathVariable String id) {
        return productReviewService.find(id);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> delete(@PathVariable String id) {
        return productReviewService.delete(id);
    }
}
