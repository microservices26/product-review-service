package com.microservices.productreview.controller;

import com.microservices.productreview.Application;
import com.microservices.productreview.exception.ApiError;
import com.microservices.productreview.model.ProductReview;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application.class)
class ProductReviewControllerTests {
    @Autowired
    ReactiveMongoTemplate mongoTemplate;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    ApplicationContext context;

    @BeforeEach
    public void clean() {
        mongoTemplate.findAllAndRemove(Query.query(new Criteria()), ProductReview.class).collectList().block();
       assertThat(mongoTemplate.findAll(ProductReview.class).collectList().block()).isEmpty();

    }

    public static Stream<Arguments> productReviewValidationsBodyCases() {
        return Stream.of(
                Arguments.of("{}",
                        Map.of(
                                "productId", List.of("must not be blank"),
                                "averageReviewScore", List.of("must not be null"),
                                "numberOfReviews", List.of("must not be null")
                        )),
                Arguments.of("{\"productId\": \"\", \"averageReviewScore\": -1, \"numberOfReviews\": -1}}",
                        Map.of(
                                "productId", List.of("must not be blank"),
                                "averageReviewScore", List.of("must be greater than or equal to 0"),
                                "numberOfReviews", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"productId\": \"as1234\", \"averageReviewScore\": 1.4, \"numberOfReviews\": -1}}",
                        Map.of(
                                "numberOfReviews", List.of("must be greater than or equal to 0")
                        ))
                );
    }

    @ParameterizedTest
    @MethodSource("productReviewValidationsBodyCases")
    public void createProductReviewValidations(String resource, Map<String, List<String>> expectedErrors) {
        ApiError response = webTestClient.put()
                .uri("/review")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ApiError.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getError()).isEqualTo("Bad Request");
        assertThat(response.getFieldErrors()).hasSameSizeAs(expectedErrors);
        response.getFieldErrors().forEach((field, errors) -> assertThat(errors).hasSameElementsAs(expectedErrors.get(field)));
    }

    @Test
    @WithMockUser
    public void createProductReview() {
        String body = "{\"productId\": \"as1234\", \"averageReviewScore\": 1.4, \"numberOfReviews\": 1}}";
        ProductReview response = webTestClient.put()
                .uri("/review")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductReview.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getProductId()).isEqualTo("as1234");
        assertThat(response.getAverageReviewScore()).isEqualTo(1.4);
        assertThat(response.getNumberOfReviews()).isEqualTo(1);

        ProductReview registry = mongoTemplate.findById(response.getProductId(), ProductReview.class).block();
        assertThat(registry).isNotNull();
        assertThat(registry.getProductId()).isEqualTo("as1234");
        assertThat(registry.getAverageReviewScore()).isEqualTo(1.4);
        assertThat(registry.getNumberOfReviews()).isEqualTo(1);
    }


    @ParameterizedTest
    @MethodSource("productReviewValidationsBodyCases")
    public void updateProductReviewValidations(String resource, Map<String, List<String>> expectedErrors) {
        ApiError response = webTestClient.put()
                .uri("/review/As2134")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ApiError.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getError()).isEqualTo("Bad Request");
        assertThat(response.getFieldErrors()).hasSameSizeAs(expectedErrors);
        response.getFieldErrors().forEach((field, errors) -> assertThat(errors).hasSameElementsAs(expectedErrors.get(field)));
    }

    @Test
    @WithMockUser
    public void updateProductReview() {
        ProductReview review = new ProductReview("As2134", 9.3, 2);
        mongoTemplate.save(review).block();

        String body = "{\"productId\": \"As2134\", \"averageReviewScore\": 1.4, \"numberOfReviews\": 1}}";
        ProductReview response = webTestClient.put()
                .uri("/review/As2134")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductReview.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getProductId()).isEqualTo("As2134");
        assertThat(response.getAverageReviewScore()).isEqualTo(1.4);
        assertThat(response.getNumberOfReviews()).isEqualTo(1);

        ProductReview registry = mongoTemplate.findById(response.getProductId(), ProductReview.class).block();
        assertThat(registry).isNotNull();
        assertThat(registry.getProductId()).isEqualTo("As2134");
        assertThat(registry.getAverageReviewScore()).isEqualTo(1.4);
        assertThat(registry.getNumberOfReviews()).isEqualTo(1);
    }

    @Test
    public void notFoundWhenFindNonExistentProductReview() {
        ProductReview review = new ProductReview("As2134", 9.3, 2);
        mongoTemplate.save(review).block();

        ApiError response = webTestClient.get()
                .uri("/review/NONEXISTENT")
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(ApiError.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getError()).isEqualTo("Product Review Not Found (id: NONEXISTENT)");
        assertThat(response.getFieldErrors()).isEmpty();
    }

    @Test
    public void findProductReview() {
        ProductReview review = new ProductReview("As2134", 9.3, 2);
        mongoTemplate.save(review).block();

        ProductReview response = webTestClient.get()
                .uri("/review/" + review.getProductId())
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductReview.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getProductId()).isEqualTo("As2134");
        assertThat(response.getAverageReviewScore()).isEqualTo(9.3);
        assertThat(response.getNumberOfReviews()).isEqualTo(2);
    }

    @Test
    public void emptyIfListWithoutProductReviews() {
        List<ProductReview> response = webTestClient.get()
                .uri("/review")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ProductReview.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response).isEmpty();
    }

    @Test
    public void listProductReviews() {
        ProductReview review1 = new ProductReview("fd5435", 3.2, 2);
        ProductReview review2 = new ProductReview("jg8674", 4.2, 3);
        mongoTemplate.save(review1).block();
        mongoTemplate.save(review2).block();

        List<ProductReview> response = webTestClient.get()
                .uri("/review")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ProductReview.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.size()).isEqualTo(2);
        assertThat(response.stream().anyMatch(r -> r.getProductId().equals("fd5435") &&
                r.getAverageReviewScore() == 3.2 &&
                r.getNumberOfReviews() == 2));
        assertThat(response.stream().anyMatch(r -> r.getProductId().equals("jg8674") &&
                r.getAverageReviewScore() == 4.2 &&
                r.getNumberOfReviews() == 3));
    }

    @Test
    @WithMockUser
    public void notFoundWhenDeleteNonExistentProductReview() {
        ProductReview review = new ProductReview("as4321", 1.2, 2);
        mongoTemplate.save(review).block();

        ApiError response = webTestClient.delete()
                .uri("/review/NONEXISTENT")
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(ApiError.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getError()).isEqualTo("Product Review Not Found (id: NONEXISTENT)");
        assertThat(response.getFieldErrors()).isEmpty();
    }

    @Test
    @WithMockUser
    public void deleteProductReview() {
        ProductReview review = new ProductReview("as4321", 1.2, 2);
        mongoTemplate.save(review).block();

        webTestClient.delete()
                .uri("/review/" + review.getProductId())
                .exchange()
                .expectStatus().isOk();

        ProductReview registry = mongoTemplate.findById(review.getProductId(), ProductReview.class).block();
        assertThat(registry).isNull();
    }
}
