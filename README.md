[![pipeline status](https://gitlab.com/microservices26/product-review-service/badges/master/pipeline.svg)](https://gitlab.com/microservices26/product-review-service/-/commits/master)
[![coverage report](https://gitlab.com/microservices26/product-review-service/badges/master/coverage.svg)](https://gitlab.com/microservices26/product-review-service/-/commits/master)

# Useful commands
#### Run database
``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up -d database
```

### Run locally  
- Run the database.
- Run the Application main class. 
#### swagger
After running locally swagger documentation is in http://localhost:8080/swagger-ui.html

### build docker 
```
docker build -f cicd/build/Dockerfile .
```

### build with docker compose
```
docker-compose -f cicd/docker-compose.yml --project-directory . build product-review
```

### run with docker compose
``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up -d
``` 

> *NOTE:* run  these commands from the project directory
